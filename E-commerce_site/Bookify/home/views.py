from django.shortcuts import render,HttpResponse
from datetime import datetime
from home.models import Contact
from django.contrib import messages

# Create your views here.
def index(request):
    return render(request,'index.html')
    #return HttpResponse("THIS IS HOMEPAGE")

def about(request):
    #return HttpResponse("THIS IS ABOUTPAGE")    
    return render(request,'about.html')

def services(request):
    #return HttpResponse("THIS IS service page")     
    return render(request,'services.html')
def contact(request):
    if request.method == "POST":
        name= request.POST.get('name')
        email= request.POST.get('email')
        phone= request.POST.get('phone')
        ##desc= request.POST.get('desc')
        contact=Contact(name=name,email=email,phone=phone,date=datetime.today())
        contact.save()
        messages.success(request, 'Your Message has been sent!!')
    #return HttpResponse("THIS IS CONTACT page")    
    return render(request,'contact.html')